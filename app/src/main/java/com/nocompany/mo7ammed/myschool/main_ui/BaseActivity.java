package com.nocompany.mo7ammed.myschool.main_ui;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

public class BaseActivity extends AppCompatActivity{





    public void showLongToast(String message){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    public void showShortToast(String message){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }


    public void testLog(String logMessage){
        Log.d("test",logMessage);
    }
}
