package com.nocompany.mo7ammed.myschool.main_ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;

import java.util.Locale;

public class DispatcherActivity extends BaseActivity {

    public static final String CHANGING_LANGUAGE = "changing_language";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeApplicationLanguage();
        runDispatcher();
    }

    private void runDispatcher() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void changeApplicationLanguage() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        int langID = Integer.parseInt(sharedPref.getString(SettingsActivity.LANGUAGE, "0"));

        Locale locale;
        if (langID ==1)
            locale = new Locale("ar");
        else
            locale = new Locale("en");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}
